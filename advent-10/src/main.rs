use std::io::stdin;

fn main() {
    let mut values: Vec<i32> = Vec::new();
    let mut value = 1;

    values.push(1);

    for line in stdin().lines().flatten() {
        let parts: Vec<&str> = line.split_whitespace().collect();
        match parts.as_slice() {
            &["noop"] => {
                values.push(value);
            },
            &["addx", val] => {
                values.push(value);
                value += val.parse::<i32>().unwrap();
                values.push(value);
            }
            &_ => {}
        }
    }

    let total: i32 = (20..=220).step_by(40)
        .map(|i| {
            let value = values.get(i-1).unwrap() * i as i32;
            println!("{i}: {value}");
            value
        })
        .sum();
    println!("{total}");

    for line in values.chunks(40).filter(|line| line.len() == 40) {
        for (col, x) in line.iter().enumerate() {
            let char = if (col as i32 - x).abs() <= 1 { '█' } else { '░' };
            print!("{char}");
        }
        println!();
    }
}
