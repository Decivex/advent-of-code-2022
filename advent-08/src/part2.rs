use std::io;
use std::io::stdin;
use itertools::{iproduct, Itertools};
use crate::coords::{Coords, DIRECTIONS};
use crate::grid::Grid;

mod grid;
mod coords;


fn scenic_score(grid: &Grid<i32>, coords: Coords) -> i32 {
    let mut score: i32 = 1;
    let height = grid.get(coords).unwrap();

    for &direction in DIRECTIONS {
        let mut direction_score = 0;

        for h in coords
                .iter_direction(direction)
                .skip(1)
                .map(|c| grid.get(c))
                .while_some() {

            direction_score += 1;

            if h >= height {
                break;
            }
        }

        score *= direction_score;
    }
    score
}


fn main() -> io::Result<()> {
    let lines = stdin().lines().collect::<io::Result<Vec<String>>>()?;
    let mut tree_grid: Grid<i32> = Grid::from_size(
        lines[0].chars().count(),
        lines.len()
    );

    for (row, line) in lines.iter().enumerate() {
        let row_data = line
            .chars()
            .map(|c| c.to_digit(10).unwrap() as i32)
            .collect_vec();

        tree_grid.set_row(row, row_data.as_slice());
    }

    println!("{tree_grid}");

    let (w, h) = tree_grid.dimensions();

    let mut scenic_scores: Grid<i32> = Grid::from_size(w, h);

    for coords in iproduct!(0..w, 0..h) {
        let coords: Coords = coords.into();
        *scenic_scores.get_mut(coords).unwrap() = scenic_score(&tree_grid, coords)
    }

    println!("{scenic_scores}");

    println!("{}", scenic_scores.max());

    Ok(())
}