use std::fmt::{Display, Formatter};
use itertools::Itertools;
use crate::coords::Coords;

pub struct Grid<T> {
    data: Vec<T>,
    width: usize,
    height: usize
}

impl<T> Grid<T> {
    fn get_idx(&self, coords: Coords) -> Option<usize> {
        let x = coords.x() as usize;
        let y = coords.y() as usize;
        if !(0..self.width).contains(&x)
        || !(0..self.height).contains(&y) {
            None
        }
        else {
            Some(y * self.width + x)
        }
    }

    pub fn get(&self, coords: Coords) -> Option<&T> {
        self.data.get(self.get_idx(coords)?)
    }

    pub fn get_mut(&mut self, coords: Coords) -> Option<&mut T> {
        let idx = self.get_idx(coords)?;
        self.data.get_mut(idx)
    }

    pub fn dimensions(&self) -> (usize, usize) {
        (self.width, self.height)
    }
}

impl<T> Grid<T> where T: Copy {
    pub fn set_row(&mut self, row: usize, data: &[T]) {
        if data.len() != self.width {
            panic!(
                "Expected slice of length {}, got {}",
                self.width,
                data.len()
            );
        }

        let start = row * self.width;
        let end = start + self.width;
        self.data.as_mut_slice()[start..end].copy_from_slice(data);
    }
}

impl<T> Grid<T> where T: Default + Clone {
    pub fn from_size(width: usize, height: usize) -> Self {
        Self {
            data: vec![T::default(); width * height],
            width,
            height
        }
    }
}

impl Grid<i32> {
    pub fn max(&self) -> i32 {
        *self.data.iter().max().unwrap()
    }
}

impl Display for Grid<i32> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let single_digits = self.max() < 10;

        let formatter = if single_digits {
            |digit: &i32| if *digit < 0 { "-".to_string() } else { digit.to_string() }
        } else {
            |digit: &i32| digit.to_string()
        };

        let item_separator = if single_digits { "" } else { ",\t" };

        let s = self.data
            .as_slice()
            .chunks(self.width)
            .into_iter()
            .map(|row| row
                .iter()
                .map(formatter)
                .join(item_separator)
            )
            .join("\n");
            write!(f, "{s}")
    }
}