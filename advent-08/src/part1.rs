use std::io::stdin;
use itertools::Itertools;


type TreeCell = [i32; 5];


fn is_visible(tree: &TreeCell) -> bool {
    for shadow in &tree[1..] {
        let height = &tree[0];
        if shadow < height {
            return true;
        }
    }

    return false;
}


fn value_show(val: i32) -> String {
    if val >= 0 {
        val.to_string()
    }
    else {
        "-".to_string()
    }
}


fn print_grid(grid: &Vec<TreeCell>, width: usize, field: usize) {
    grid.chunks(width).for_each(|row| {
        let row_str = row
            .iter()
            .map(|cell| value_show(cell[field]))
            .join("");
        println!("{row_str}");
    });
}


fn main() {
    let grid: Vec<Vec<u32>> = stdin().lines().flatten().map(|line| {
        line.chars().map(|c| c.to_digit(10).unwrap()).collect()
    }).collect();

    let width = grid[0].len();
    let height = grid.len();

    println!("Grid dimensions: {width}x{height}");

    let mut data: Vec<TreeCell> = grid.into_iter().flatten().map(|h| [h as i32, -1, -1, -1, -1]).collect();

    let directions: [(usize, usize, usize, usize, bool); 4] = [
        // outer step size, outer_limit, inner step size, limit, reverse
        (1usize, height, width, height, false), // Down
        (width, width, 1usize, width, false), // Right
        (1usize, height, width, height, true), // Up
        (width, width, 1usize, width, true) // Left
    ];

    for (shadow_offset, &(outer_step, outer_limit, inner_step, inner_limit, reverse)) in directions.iter().enumerate() {
        for initial_idx in (0..).step_by(outer_step).take(outer_limit) {
            let mut indices: Vec<usize> = (initial_idx..)
                .step_by(inner_step)
                .take(inner_limit)
                .collect();

            if reverse {
                indices.reverse();
            }

            for (&prev, &idx) in indices.iter().tuple_windows() {
                let prev_shadow: i32 = data[prev][shadow_offset+1];
                let prev_height: i32 = data[prev][0];
                let shadow: &mut i32 = &mut data[idx][shadow_offset+1];
                *shadow = i32::max(prev_shadow, prev_height);
            }
        }
    }

    println!("Grid:");
    print_grid(&data, width, 0);

    println!();

    println!("Down:");
    print_grid(&data, width, 1);

    println!();

    println!("Right:");
    print_grid(&data, width, 2);

    println!();

    println!("Up:");
    print_grid(&data, width, 3);

    println!();

    println!("Left:");
    print_grid(&data, width, 4);

    println!();

    let visibillity_data: Vec<bool> = data.iter().map(is_visible).collect();

    println!("Visible:");
    visibillity_data
        .iter()
        .map(|&visible| if visible { "*" } else { "." })
        .chunks(width)
        .into_iter()
        .map(|row| row.into_iter().join(""))
        .for_each(|line| println!("{line}"));

    let count = visibillity_data.iter().filter(|&&b| b).count();
    println!("{count}")

}
