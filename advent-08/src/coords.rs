use std::fmt::{Display, Formatter};
use crate::coords::Direction::{Down, Left, Right, Up};

#[derive(Clone, Copy)]
pub struct Coords {
    x: isize,
    y: isize
}

#[derive(Clone, Copy)]
pub enum Direction {
    Up, Down, Left, Right
}

pub const DIRECTIONS: &[Direction] = &[Up, Down, Left, Right];

impl Coords {
    pub fn new(x: isize, y: isize) -> Self {
        Self { x, y }
    }

    pub fn x(&self) -> isize {
        self.x
    }

    pub fn y(&self) -> isize {
        self.y
    }

    pub fn offset(mut self, direction: Direction) -> Self {
        match direction {
            Up => { self.y -= 1; }
            Down => { self.y += 1 }
            Left => { self.x -= 1 }
            Right => { self.x += 1 }
        }
        self
    }

    pub fn iter_direction(&self, direction: Direction) -> DirectionalIterator {
        DirectionalIterator { current: self.clone(), direction }
    }
}

pub struct DirectionalIterator {
    current: Coords,
    direction: Direction
}

impl Iterator for DirectionalIterator {
    type Item = Coords;

    fn next(&mut self) -> Option<Self::Item> {
        let current = self.current;
        self.current = current.offset(self.direction);
        Some(current)
    }
}

impl From<(usize, usize)> for Coords {
    fn from(coords: (usize, usize)) -> Self {
        Self::new(coords.0 as isize, coords.1 as isize)
    }
}

impl Display for Coords {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}