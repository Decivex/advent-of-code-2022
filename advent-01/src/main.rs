use itertools::Itertools;

fn main() {
    let max = std::io::stdin()
        .lines()
        .flatten()
        .map(|s| s.trim().to_string())
        .group_by(|s| s.is_empty())
        .into_iter()
        .filter(|(blank, _)| blank == &false)
        .map(|(_, group)| group
            .map(|s| s.parse::<i32>().expect("error parsing number"))
            .sum::<i32>()
        )
        .sorted()
        .rev()
        .take(3)
        .sum::<i32>();
    println!("{max}")
}
