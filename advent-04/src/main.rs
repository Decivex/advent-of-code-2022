use std::io::{Read, stdin};
use std::ops::RangeInclusive;

use nom::character::complete::{newline, char, u32 as uint};
use nom::IResult;
use nom::multi::separated_list0;
use nom::sequence::separated_pair;

type UIntRange = RangeInclusive<u32>;

fn range(input: &str) -> IResult<&str, UIntRange> {
    let (input, start) = uint(input)?;
    let (input, _) = char('-')(input)?;
    let (input, end) = uint(input)?;
    Ok((input, start..=end))
}

fn range_pair(input: &str) -> IResult<&str, (UIntRange, UIntRange)> {
    Ok(separated_pair(range, char(','), range)(input)?)
}

fn ranges_overlap(r1: &UIntRange, r2: &UIntRange) -> bool {
    let first_end = r1.end().min(r2.end());
    let second_start = r1.start().max(r2.start());
    first_end >= second_start
}

fn main() {
    let mut input = String::new();
    stdin().read_to_string(&mut input).unwrap();
    let (_, list) = separated_list0(newline, range_pair)(&input).unwrap();

    let count = list.into_iter().filter(|(r1, r2)| ranges_overlap(r1, r2)).count();

    println!("{count}");
}
