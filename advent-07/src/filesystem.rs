use std::collections::BTreeMap;
use std::io;


pub type Path<'a> = Vec<&'a str>;


#[derive(Debug)]
pub enum FsEntry {
    File(usize),
    Dir(BTreeMap<String, FsEntry>)
}


pub struct DirIterator<'a> {
    stack: Vec<(Path<'a>, &'a FsEntry)>
}


impl<'a> Iterator for DirIterator<'a> {
    type Item = (Path<'a>, &'a FsEntry);

    fn next(&mut self) -> Option<Self::Item> {
        let (path, entry) = self.stack.pop()?;

        if let FsEntry::Dir(entries) = entry {
            for (name, fs_entry) in entries.iter().rev() {
                let mut next_path = path.clone();
                next_path.push(name);
                self.stack.push((next_path, fs_entry));
            }
        }

        Some((path, entry))
    }
}


impl FsEntry {
    pub fn get_path_mut(&mut self, path: &Path) -> io::Result<&mut Self> {
        let mut current = self;

        for &part in path {
            match current {
                FsEntry::File(_) => return Err(io::Error::new(
                    io::ErrorKind::Other,
                    "File is not a directory."
                )),
                FsEntry::Dir(entries) => {
                    match entries.get_mut(part) {
                        None => return Err(io::Error::new(
                            io::ErrorKind::NotFound,
                            "Path not found."
                        )),
                        Some(next) => {
                            current = next;
                        }
                    }
                }
            }
        }

        Ok(current)
    }

    pub fn print(&self) {
        self.print_level("/", 0);
    }

    fn print_level(&self, name: &str, indent_level: usize) {
        let indent = "  ".repeat(indent_level).to_string();
        match self {
            FsEntry::File(size) => {
                println!("{indent}- {name} (file, size={size})");
            },
            FsEntry::Dir(entries) => {
                println!("{indent}- {name} (dir)");
                for (name, entry) in entries.iter() {
                    entry.print_level(name, indent_level+1)
                }
            }
        }
    }

    pub fn iter(&self) -> DirIterator {
        DirIterator {
            stack: vec![(vec![""], self)]
        }
    }

    pub fn total_size(&self) -> usize {
        match self {
            FsEntry::File(size) => size.clone(),
            FsEntry::Dir(entries) => entries
                .values()
                .map(Self::total_size)
                .sum()
        }
    }

    pub fn is_dir(&self) -> bool {
        match self {
            FsEntry::File(_) => false,
            FsEntry::Dir(_) => true
        }
    }
}