use std::collections::BTreeMap;
use std::io::{Read, stdin};
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{char, newline, u64 as uint64};
use nom::character::streaming::not_line_ending;
use nom::combinator::map;
use nom::IResult;
use nom::multi::{separated_list0, separated_list1};
use nom::sequence::{preceded, separated_pair};
use crate::filesystem::{FsEntry, Path};

mod filesystem;

#[derive(Debug)]
enum LSOutput<'a> {
    File(&'a str, usize),
    Dir(&'a str)
}

impl<'a> LSOutput<'a> {
    fn name(&'a self) -> &'a str {
        match self {
            LSOutput::File(name, _) => name,
            LSOutput::Dir(name) => name
        }
    }
}


#[derive(Debug)]
enum Command<'a> {
    Cd(&'a str),
    Ls(Vec<LSOutput<'a>>)
}

fn cd_command(input: &str) -> IResult<&str, &str> {
    preceded(tag("$ cd "), not_line_ending)(input)
}

fn ls_command(input: &str) -> IResult<&str, Vec<LSOutput>> {
    preceded(tag("$ ls\n"), separated_list0(
        newline, alt((
            map(preceded(tag("dir "), not_line_ending), |name| LSOutput::Dir(name)),
            map(separated_pair(uint64, char(' '), not_line_ending), |(size, name)| LSOutput::File(name, size as usize))
        ))
    ))(input)
}

fn command(input: &str) -> IResult<&str, Command> {
    alt((
        map(cd_command, Command::Cd),
        map(ls_command, Command::Ls)
    ))(input)
}

fn command_list(input: &str) -> IResult<&str, Vec<Command>> {
    separated_list1(newline, command)(input)
}

impl<'a> From<LSOutput<'a>> for FsEntry {
    fn from(lso: LSOutput) -> Self {
        match lso {
            LSOutput::File(_, size) => FsEntry::File(size),
            LSOutput::Dir(_) => FsEntry::Dir(BTreeMap::new())
        }
    }
}


const TOTAL_SIZE: usize = 70_000_000;
const REQUIRED_SPACE: usize = 30_000_000;


fn main() {
    let mut input = String::new();
    stdin().read_to_string(&mut input).unwrap();

    let (_, cmdlist) = command_list(&input).unwrap();

    let mut cwd = Path::new();
    let mut root = FsEntry::Dir(BTreeMap::new());

    for command in cmdlist {
        match command {
            Command::Cd(path) => {
                match path {
                    ".." => {
                        cwd.pop().unwrap();
                    },
                    "/" => {
                        cwd.clear();
                    }
                    dirname => {
                        cwd.push(dirname);
                    }
                }
            }
            Command::Ls(output) => {
                match root.get_path_mut(&cwd).unwrap() {
                    FsEntry::File(_) => panic!("Tried creating files in file"),
                    FsEntry::Dir(entries) => {
                        for entry in output {
                            entries.insert(entry.name().to_string(), entry.into());
                        }
                    }
                }
            }
        }
    }

    root.print();

    let free_space = TOTAL_SIZE - root.total_size();
    let extra_space_required = REQUIRED_SPACE - free_space;

    let answer: usize = root.iter()
        .filter(|(_, entry)| entry.is_dir())
        .map(|(_, entry)| entry.total_size())
        .filter(|&size| size >= extra_space_required)
        .min()
        .unwrap();

    println!("{answer}");
}