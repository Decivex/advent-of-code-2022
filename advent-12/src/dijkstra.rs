use std::collections::{HashMap, HashSet, VecDeque};
use crate::map::{Coords, determine_direction, Direction, TileMap};
use crate::map::Tile::{End, Start};

#[derive(Debug)]
pub struct Dijkstra<'a> {
    map: &'a TileMap,
    /// Distance from start for each node
    distance: HashMap<Coords, usize>,
    /// Reversed direction of each node
    pub direction: HashMap<Coords, Direction>,
    queue: VecDeque<Coords>,
    seen: HashSet<Coords>
}

impl<'a> Dijkstra<'a> {
    pub fn new(map: &'a TileMap) -> Dijkstra<'a> {
        let mut distance = HashMap::new();
        let source = HashMap::new();

        let start = map.find_tile(&Start).expect("No start tile found!");
        distance.insert(start, 0);

        Dijkstra {
            map, distance, direction: source, queue: VecDeque::from([start]), seen: HashSet::new()
        }
    }

    pub fn advance(&mut self) -> Option<usize> {
        let next = self.queue.pop_front().expect("Queue empty");

        if self.seen.contains(&next) {
            return None;
        }

        let distance = self.distance.get(&next)
            .expect(&format!("Distance to {next:?} is unknown.")) + 1;

        for neighbor in self.map.get_reachable_from(&next) {
            if self.map.coords(&neighbor) == Some(&End) {
                return Some(distance);
            }

            let rdir = determine_direction(&neighbor, &next)
                .expect(&format!("Can't determine direction between {next:?} and {neighbor:?}."));

            if self.distance.get(&neighbor).map_or(true, |pdist| distance < *pdist) {
                self.direction.insert(neighbor, rdir);
                self.distance.insert(neighbor, distance);
                self.queue.push_back(neighbor);
            }
        }

        None
    }
}

