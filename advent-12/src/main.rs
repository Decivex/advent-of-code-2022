use std::io::stdin;
use std::thread;
use std::time::Duration;
use ansi_term::Color;
use Color::{Black, Green, Purple, Yellow};
use crate::dijkstra::Dijkstra;
use crate::display::Cell;
use crate::map::{coords, Direction, Tile, TileMap};

pub mod map;
pub mod dijkstra;
pub mod display;

fn height_color(h: u8) -> Color {
    let c = 42 + h * 7;
    Color::RGB(c, c, c)
}

fn tile_cell(tile: &Tile) -> Cell {
    match tile {
        Tile::Start => Cell {
            fg: Black,
            bg: Yellow,
            char: '@'
        },
        Tile::Height(h) => Cell {
            fg: height_color(*h),
            bg: height_color(*h + 1),
            char: char::from(h + 'a' as u8)
        },
        Tile::End => Cell {
            fg: Black,
            bg: Green,
            char: '⚑'
        }
    }
}

fn map_cells(map: &TileMap) -> Vec<Vec<Cell>> {
    map.rows.iter().map(|cols| {
        cols.iter().map(tile_cell).collect()
    }).collect()
}

fn print_cells(cells: &Vec<Vec<Cell>>) {
    let str: String = cells.iter().map(|cols| {
        let line: String = cols.iter().map(Cell::to_string).collect();
        line + "\n"
    }).collect();
    println!("{str}");
}

fn annotate_cell_map(cells: &mut Vec<Vec<Cell>>, dijkstra: &Dijkstra) {
    cells.iter_mut().enumerate().for_each(|(y, row)| {
        row.iter_mut().enumerate().for_each(|(x, cell)| {
            let coords = coords(x, y);
            if let Some(direction) = dijkstra.direction.get(&coords) {
                cell.fg = Purple;
                cell.char = match direction {
                    Direction::North => '↑',
                    Direction::East => '→',
                    Direction::South => '↓',
                    Direction::West => '←'
                };
            }
        });
    });
}

fn main() {
    let map: TileMap = stdin().lines().into();

    let delay = std::env::var("DELAY")
        .ok()
        .map(|s| s.parse::<u64>().ok())
        .flatten()
        .unwrap_or(500);


    let mut dijkstra = Dijkstra::new(&map);

    loop {
        thread::sleep(Duration::from_millis(delay));
        let mut cells = map_cells(&map);
        annotate_cell_map(&mut cells, &dijkstra);

        print!("\x1b[0;0f");
        print_cells(&cells);
        print!("\x1b[J");
        if let Some(distance) = dijkstra.advance() {
            println!("Distance: {distance}");
            return;
        }
    }
}
