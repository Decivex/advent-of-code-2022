use ansi_term::Color;

pub struct Cell {
    pub fg: Color,
    pub bg: Color,
    pub char: char
}

impl ToString for Cell {
    fn to_string(&self) -> String {
        self.fg.on(self.bg).paint(self.char.to_string()).to_string()
    }
}