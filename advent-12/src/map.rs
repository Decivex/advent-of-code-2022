use std::fmt::{Debug, Formatter};
use std::io::{BufRead, Lines};
use crate::map::Direction::{East, North, South, West};
use crate::map::Tile::{End, Height, Start};

pub type Coords = (isize, isize);

const CARDINAL_OFFSETS: &[Coords] = &[
    ( 0, -1),
    ( 1,  0),
    ( 0,  1),
    (-1,  0)
];

#[derive(Debug)]
pub enum Direction {
    North,
    East,
    South,
    West
}

impl Direction {
    pub fn offset(&self) -> Coords {
        match self {
            North => (0, -1),
            East => (1, 0),
            South => (0, 1),
            West => (-1, 0)
        }
    }
}

pub fn determine_direction(a: &Coords, b: &Coords) -> Option<Direction> {
    let offset = (b.0 - a.0, b.1 - a.1);

    match offset {
        (..=-1, 0) => Some(West),
        (1.., 0) => Some(East),
        (0, ..=-1) => Some(North),
        (0, 1..) => Some(South),
        _ => None
    }
}

pub fn coords(x: usize, y: usize) -> Coords { (x as isize, y as isize) }

pub fn add_offset(a: &Coords, b: &Coords) -> Coords {
    (a.0 + b.0, a.1 + b.1)
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Debug)]
pub enum Tile {
    Start,
    Height(u8),
    End
}

impl Tile {
    fn can_pass_to(&self, other: &Tile) -> bool {
        let result = match (self, other) {
            (Start, Height(0)) => true,
            (Height(25), End) => true,
            (Height(a), Height(b)) => *b <= *a + 1,
            (_, _) => false
        };
        println!("{self:?} -> {other:?} : {result}");
        result
    }
}

impl From<char> for Tile {
    fn from(value: char) -> Self {
        match value {
            'S' => Start,
            'E' => End,
            'a'..='z' => Height(value as u8 - 'a' as u8),
            _ => panic!("Unparseable character '{value}'")
        }
    }
}

pub struct TileMap {
    pub rows: Vec<Vec<Tile>>
}

impl Debug for TileMap {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<TileMap>")
    }
}

impl TileMap {
    pub fn get(&self, x: usize, y: usize) -> Option<&Tile> {
        self.rows.get(y)?.get(x)
    }

    pub fn coords(&self, coords: &Coords) -> Option<&Tile> {
        if coords.0 < 0 || coords.1 < 0 {
            return None;
        }

        return self.get(coords.0 as usize, coords.1 as usize)
    }

    pub fn find_tile(&self, tile: &Tile) -> Option<Coords> {
        self.rows.iter().enumerate().map(|(y, cols)| {
            cols.iter()
                .enumerate()
                .find(|(_, t)| t == &tile)
                .map(|(x, _)| coords(x, y))
        }).flatten().next()
    }

    pub fn all_coords(&self) -> Vec<Coords> {
        self.rows.iter()
            .map(Vec::len)
            .enumerate()
            .flat_map(|(y, len)| (0..len).map(move |x| coords(x, y)))
            .collect()
    }

    pub fn get_reachable_from(&self, coords: &Coords) -> Vec<Coords> {
        self.coords(coords).map_or(vec![], |tile| {
            CARDINAL_OFFSETS.iter()
                .map(|offset| add_offset(coords, offset))
                .filter(|c| {
                    self.coords(c).map_or(false, |t| tile.can_pass_to(t))
                })
                .collect()
        })
    }
}

impl<T> From<Lines<T>> for TileMap where T: BufRead {
    fn from(lines: Lines<T>) -> Self {
        let rows = lines
            .map(|result| parse_line(&result.expect("Error reading line.")));
        TileMap {
            rows: rows.collect()
        }
    }
}

pub fn parse_line(line: &str) -> Vec<Tile> {
    line.chars().map(|c| c.into()).collect()
}

#[cfg(test)]
mod tests {
    use crate::map::{parse_line, Tile, TileMap};
    use crate::map::Tile::{End, Height, Start};

    #[test]
    fn char_conversion() {
        assert_eq!(Start, Tile::from('S'));
        assert_eq!(End, Tile::from('E'));
        assert_eq!(Height(0), Tile::from('a'));
        assert_eq!(Height(1), Tile::from('b'));
        assert_eq!(Height(2), Tile::from('c'));
        assert_eq!(Height(23), Tile::from('x'));
        assert_eq!(Height(24), Tile::from('y'));
        assert_eq!(Height(25), Tile::from('z'));
    }

    #[test]
    fn find_tiles() {
        let lines = include_str!("../test_input.txt").lines().map(parse_line);
        let map = TileMap {
            rows: lines.collect()
        };

        assert_eq!(Some((0, 0)), map.find_tile(&Start));
        assert_eq!(Some((5, 2)), map.find_tile(&End));
        assert_eq!(Some((3, 0)), map.find_tile(&Height(16)));
        assert_eq!(None, map.find_tile(&Height(26)));
    }

    #[test]
    fn get_neighbours() {
        let lines = include_str!("../test_input.txt").lines().map(parse_line);
        let map = TileMap {
            rows: lines.collect()
        };

        let neighbours = map.get_reachable_from(&(4, 4));
        assert_eq!(vec![
            (5, 4),
            (3, 4)
        ], neighbours);
    }
}