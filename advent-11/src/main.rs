use std::collections::BTreeSet;
use std::io::{Read, stdin};
use log::{debug, info};
use nom::character::complete::newline;
use nom::multi::{many1, separated_list0};
use num::{Integer, One};
use crate::monkey::{Item, Monkey};
use crate::parse::monkey;

mod expr;
mod parse;
mod monkey;


const ROUNDS: usize = 10_000;
const WORRY_LEVEL_DIVISION: u128 = 1;


fn inspect_items(monkey: &mut Monkey, max_worry: &Item) -> Vec<(usize, Item)> {
    monkey.inspection_count += monkey.items.len();
    monkey.items.drain(0..).map(|item| {
        let new_value = (monkey.operation.get(&item) / WORRY_LEVEL_DIVISION).mod_floor(max_worry);
        let destination = if new_value.is_multiple_of(&monkey.test) {
            monkey.target_true
        } else {
            monkey.target_false
        };
        debug!("{destination} -> {new_value}");
        (destination, new_value)
    }).collect()
}


fn play_round(monkeys: &mut [Monkey], max_worry: &Item) {
    for idx in 0..monkeys.len() {
        for (destination, item) in inspect_items(&mut monkeys[idx], max_worry) {
            monkeys[destination].items.push(item);
        }
    }
}


fn main() {
    env_logger::init();
    let mut input = String::new();
    stdin().read_to_string(&mut input).unwrap();

    let (_, mut monkeys) = separated_list0(many1(newline), monkey)(&input).unwrap();

    let max_worry = monkeys.iter().map(|monkey| &monkey.test).fold(Item::one(), |a, b| a*b);
    info!("Max worry: {max_worry}");

    debug!("{monkeys:?}");
    for round in 0..ROUNDS {
        play_round(&mut monkeys, &max_worry);
    }
    debug!("{monkeys:?}");

    let inspection_counts: BTreeSet<usize> = monkeys
        .into_iter()
        .map(|monkey| monkey.inspection_count)
        .collect();

    let mut it = inspection_counts.into_iter().rev();
    let first = it.next().unwrap();
    let second = it.next().unwrap();
    println!("{}", first * second);
}
