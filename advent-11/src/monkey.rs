use crate::expr::ExprNode;


pub type Item = num::BigUint;


#[derive(Debug, Eq, PartialEq)]
pub struct Monkey {
    pub id: u32,
    pub items: Vec<Item>,
    pub operation: ExprNode,
    pub test: Item,
    pub target_true: usize,
    pub target_false: usize,
    pub inspection_count: usize
}