use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::IResult;
use nom::character::complete::{char, u128 as uverylong, space0};
use nom::combinator::{map, value as val};
use nom::sequence::separated_pair;
use crate::Item;
use crate::parse::biguint;


#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum Operation {
    Add, Mul
}


#[derive(Clone, Eq, PartialEq, Debug)]
pub enum ExprNode {
    Var,
    Val(Item),
    Operation(Box<ExprNode>, Box<ExprNode>, Operation),
}


fn number(input: &str) -> IResult<&str, ExprNode> {
    map(biguint, ExprNode::Val)(input)
}


fn var(input: &str) -> IResult<&str, ExprNode> {
    val(ExprNode::Var, tag("old"))(input)
}


fn value(input: &str) -> IResult<&str, ExprNode> {
    alt((number, var))(input)
}


fn operator(input: &str) -> IResult<&str, Operation> {
    alt((
        val(Operation::Add, char('+')),
        val(Operation::Mul, char('*'))
    ))(input)
}


pub fn operation(input: &str) -> IResult<&str, ExprNode> {
    let (input, (a, (op, b))) = separated_pair(
        value,
        space0,
        separated_pair(operator, space0,value)
    )(input)?;
    Ok((
        input,
        ExprNode::Operation(
            Box::new(a),
            Box::new(b),
            op
        )
    ))
}


impl ExprNode {
    pub(crate) fn get(&self, old: &Item) -> Item {
        match self {
            ExprNode::Var => old.clone(),
            ExprNode::Val(i) => i.clone(),
            ExprNode::Operation(a, b, op) => match op {
                Operation::Add => a.get(&old) + b.get(&old),
                Operation::Mul => a.get(&old) * b.get(&old)
            }
        }
    }
}


#[cfg(test)]
mod tests {
    use crate::expr::{ExprNode, operation, Operation};
    use crate::Item;

    #[test]
    fn parse_operation() {
        let (_, expr) = operation("6 * old").unwrap();
        assert_eq!(ExprNode::Operation(
            Box::new(ExprNode::Val(Item::from(6u32))),
            Box::new(ExprNode::Var),
            Operation::Mul
        ), expr);
        assert_eq!(Item::from(42u32), expr.get(&Item::from(7u32)));
    }
}