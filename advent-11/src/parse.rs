use log::trace;
use nom::character::complete::{digit1, newline};
use nom::character::complete::u32 as uint;
use nom::character::complete::u128 as uverylong;
use nom::IResult;
use nom::bytes::complete::tag;
use nom::error::ParseError;
use nom::multi::separated_list0;
use nom::sequence::delimited;
use num::BigUint;
use crate::expr::operation;
use crate::Item;
use crate::monkey::Monkey;


pub fn biguint(input: &str) -> IResult<&str, Item> {
    let (input, digits) = digit1(input)?;
    match Item::parse_bytes(digits.as_bytes(), 10) {
        None => {
            let kind = nom::error::ErrorKind::Digit;
            let error = nom::error::Error::from_error_kind(input, kind);
            return Err(nom::Err::Error(error));
        },
        Some(item) => Ok((input, item))
    }
}


pub fn monkey(input: &str) -> IResult<&str, Monkey> {
    trace!("Start parsing monkey");

    let (input, id) = delimited(
        tag("Monkey "),
        uint, tag(":\n")
    )(input)?;

    trace!("Monkey id: {id}");

    let (input, items) = delimited(
        tag("  Starting items: "),
        separated_list0(tag(", "), biguint),
        newline
    )(input)?;

    trace!("Monkey starting items: {items:?}");

    let (input, operation) = delimited(
        tag("  Operation: new = "),
        operation,
        newline
    )(input)?;

    trace!("Monkey operation: {operation:?}");

    let (input, test) = delimited(
        tag("  Test: divisible by "),
        biguint,
        newline
    )(input)?;

    trace!("Monkey test: x % {test} == 0");

    let (input, target_true) = delimited(
        tag("    If true: throw to monkey "),
        uint,
        newline
    )(input)?;

    trace!("Monkey target: {target_true} if true");

    let (input, target_false) = delimited(
        tag("    If false: throw to monkey "),
        uint,
        newline
    )(input)?;

    trace!("Monkey target: {target_false} if false");

    Ok((input, Monkey {
        id,
        items,
        operation,
        test,
        target_true: target_true as usize,
        target_false: target_false as usize,
        inspection_count: 0
    }))
}


#[cfg(test)]
mod tests {
    use crate::expr::ExprNode;
    use crate::expr::ExprNode::{Val, Var};
    use crate::expr::Operation::Mul;
    use crate::Item;
    use crate::monkey::Monkey;
    use crate::parse::monkey;

    #[test]
    fn parse_monkey() {
        env_logger::init();
        let code = include_str!("../test_input.txt");
        let (_, monkey) = monkey(code).unwrap();

        assert_eq!(Monkey {
            id: 0,
            items: vec![Item::from(79u32), Item::from(98u32)],
            operation: ExprNode::Operation(
                Box::new(Var),
                Box::new(Val(Item::from(19u32))),
                Mul
            ),
            test: Item::from(23u32),
            target_true: 2,
            target_false: 3,
            inspection_count: 0
        }, monkey);
    }
}