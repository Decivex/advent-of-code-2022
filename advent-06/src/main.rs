use std::collections::BTreeSet;
use std::io::{Read, stdin};


const WINDOW_SIZE: usize = 14;


fn main() {
    let bytes: Vec<u8> = stdin().bytes().flatten().collect();
    for (idx, window) in bytes.windows(WINDOW_SIZE).enumerate() {
        let different_bytes = BTreeSet::from_iter(window.iter().cloned()).len();
        if different_bytes == WINDOW_SIZE {
            println!("{}", idx + WINDOW_SIZE);
            break;
        }
    }
}
