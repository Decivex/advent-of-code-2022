use std::io::{Read, stdin};
use nom::{
    branch::alt,
    character::complete::{
        anychar,
        newline,
        char as chr,
        space1,
        u8 as byte
    },
    combinator::{
        map,
        value
    },
    IResult,
    AsChar,
    bytes::complete::tag,
    multi::{
        count,
        separated_list1
    },
    sequence::{
        delimited,
        terminated,
        tuple
    }
};

type Instruction = (u8, u8, u8);

pub fn container(input: &str) -> IResult<&str, Option<char>> {
    let c_parser = delimited(chr('['), anychar, chr(']'));
    let e_parser = count(chr(' '), 3);
    alt((
        map(c_parser, |c| Some(c)),
        value(None, e_parser)
    ))(input)
}

pub fn container_grid(input: &str) -> IResult<&str, Vec<Vec<Option<char>>>> {
    terminated(separated_list1(newline, separated_list1(chr(' '), container)), newline)(&input)
}

pub fn container_grid_legend(input: &str) -> IResult<&str, Vec<u8>> {
    separated_list1(chr(' '), delimited(chr(' '), byte, chr(' ')))(input)
}

pub fn instruction(input: &str) -> IResult<&str, Instruction> {
    let (input, (_, _, container, _, _, _, src, _, _, _, dst)) = tuple((
        tag("move"), space1, byte, space1, tag("from"), space1, byte, space1, tag("to"), space1, byte
    ))(input)?;
    Ok((input, (container, src, dst)))
}

pub fn instruction_list(input: &str) -> IResult<&str, Vec<Instruction>> {
    separated_list1(newline, instruction)(input)
}

pub fn main() {
    let mut input = String::new();
    stdin().read_to_string(&mut input).unwrap();

    let (_, (grid, legend, _, instruction_list)) = tuple((
        container_grid, container_grid_legend, count(newline, 2), instruction_list
    ))(&input).unwrap();

    let mut stacks: Vec<Vec<char>> = legend.iter().map(|stack_no| {
        let idx = (stack_no - 1) as usize;
        grid.iter()
            .rev()
            .map(|v| v.get(idx))
            .take_while(|container| container.is_some())
            .flatten()
            .flatten()
            .cloned()
            .collect::<Vec<char>>()
    }).collect();

    instruction_list.iter().for_each(|&(count, src, dst)| {
        let src = stacks.get_mut((src - 1) as usize).unwrap();
        let mut containers = src.split_off(src.len() - count as usize);
        // containers.reverse();

        let dst = stacks.get_mut((dst - 1) as usize).unwrap();
        dst.append(&mut containers);
    });

    let output = String::from_iter(stacks.into_iter().map(|stack| stack.last().unwrap().as_char()));
    println!("{output}")
}